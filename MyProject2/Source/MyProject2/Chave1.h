// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Item.h"
#include "Chave1.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT2_API AChave1 : public AItem
{
	GENERATED_BODY()
		 
public:
	AChave1();

	virtual int Use() override;


	
	
};
