// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "EnemyAI.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT2_API AEnemyAI : public AAIController
{
	GENERATED_BODY()

		UPROPERTY(transient)
		class UBlackboardComponent *Blackboardcomp;

	UPROPERTY(transient)
		class UBehaviorTreeComponent *BehaviorComp;

public :

	virtual void Possess(APawn *InPaw) override;
	AEnemyAI();
	uint8 EnemyKeyID;
};
