// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject2.h"
#include "MyProject2GameMode.h"
#include "MyCharacter.h"
#include "MyHUD.h"


AMyProject2GameMode::AMyProject2GameMode() {

	HUDClass = AMyHUD::StaticClass();

	DefaultPawnClass = AMyCharacter::StaticClass();

}
