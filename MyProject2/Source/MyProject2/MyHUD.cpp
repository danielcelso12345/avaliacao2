// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject2.h"
#include "MyHUD.h"
#include "MyHUD.h"
#include "Engine/Font.h"
#include "Engine/Canvas.h"
#include "Kismet/GameplayStatics.h"
#include "MyCharacter.h"

AMyHUD::AMyHUD() {
	static ConstructorHelpers::FObjectFinder<UFont>
		Font(TEXT("Font'/Engine/EngineFonts/RobotoDistanceField.RobotoDistanceField'"));
	if (Font.Succeeded()) {
		HUDFont = Font.Object;
	}

	ConstructorHelpers::FObjectFinder<UTexture2D>
		ChaveTexture(TEXT("Texture2D'/Game/Pngs/ImagemIcone.ImagemIcone'"));
	if (ChaveTexture.Succeeded()) {
		Chave = ChaveTexture.Object;

	}


	ConstructorHelpers::FObjectFinder<UTexture2D>
		ObjetoTexture(TEXT("Texture2D'/Game/Pngs/Objeto.Objeto'"));
	if (ObjetoTexture.Succeeded()) {
		Objeto = ObjetoTexture.Object;

		//static ConstructorHelpers::FObjectFinder<UTexture2D>
		//	Texture(TEXT("Texture2D'/Game/Pngs/BarraDeVida.BarraDeVida'"));
		//if (Texture.Succeeded()) {
		//	MyTexture = Texture.Object;
		//}
	}
}
	void AMyHUD::DrawHUD() {

		Super::DrawHUD();

		FVector2D ScreenDimensions = FVector2D(Canvas->SizeX,
			Canvas->SizeY);

		AMyCharacter* MyCharacter = Cast<AMyCharacter>(
			UGameplayStatics::GetPlayerPawn(this, 0));

		FString LifeString = FString::Printf(TEXT("Life: %d"),
			MyCharacter->GetLife());
		DrawText(LifeString, FColor::Red, 50, 50, HUDFont);
		
		
		DrawTextureSimple(Chave, ScreenDimensions.X - Chave->GetSizeX() * 2,
			50, 1.0f, false);

		FString ChaveAmount = FString::Printf(TEXT("X %d"), MyCharacter->GetInventory().Num());
		DrawText(ChaveAmount, FColor::Red, ScreenDimensions.X - Chave->GetSizeX(),
			70, HUDFont);


		DrawTextureSimple(Objeto, ScreenDimensions.X - Objeto->GetSizeX() * 2,
			120, 1.0f, false);

		FString ObjetoAmount = FString::Printf(TEXT("X %d"), MyCharacter->GetObjeto().Num());
		DrawText(ObjetoAmount, FColor::Red, ScreenDimensions.X - Objeto->GetSizeX(),
			120, HUDFont);

		//DrawTexture(MyTexture, 200, 200, MyCharacter->GetLife() * 4,
		//	MyTexture->GetSizeY(), 0, 0, MyCharacter->GetLife() * 4,
		//	MyTexture->GetSizeY(), FLinearColor::White,
		//	EBlendMode::BLEND_Translucent, 1.0f, false, 0.0f,
			//FVector2D::ZeroVector);
	}



