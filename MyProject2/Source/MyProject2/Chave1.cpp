// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject2.h"
#include "Chave1.h"



AChave1::AChave1() {
	ConstructorHelpers::FObjectFinder<UStaticMesh>
		Mesh(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Trim_90_In.Shape_Trim_90_In'"));
	if (Mesh.Succeeded()) {
		GetMeshComp()->SetStaticMesh(Mesh.Object);
	}
	GetMeshComp()->SetWorldScale3D(FVector(0.4f, 0.4f, 0.4f));
}

int AChave1::Use() {
	return FMath::RandRange(1, 10);
}



