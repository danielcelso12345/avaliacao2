// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject2.h"
#include "Objeto.h"


AObjeto::AObjeto() {
	ConstructorHelpers::FObjectFinder<UStaticMesh>
		Mesh(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Torus.Shape_Torus'"));
	if (Mesh.Succeeded()) {
		GetMeshComp()->SetStaticMesh(Mesh.Object);
	}
	GetMeshComp()->SetWorldScale3D(FVector(0.4f, 0.4f, 0.4f));
}

int AObjeto::Use2() {
	return FMath::RandRange(1, 10);
}

