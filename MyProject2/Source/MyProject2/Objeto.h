// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Item.h"
#include "Objeto.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT2_API AObjeto : public AItem
{
	GENERATED_BODY()
	

public:
	AObjeto();

	virtual int Use2() override;

	
	
};
