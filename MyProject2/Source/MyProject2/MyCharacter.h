// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "MyCharacter.generated.h"

UCLASS()
class MYPROJECT2_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;


	virtual void Jump() override;
	virtual void Crouch(bool bClientSimulation = true) override;
	virtual void StopCrouch();
	virtual void StartCrouch();
	bool TemChave;
	bool TemObjeto;

	void SetLife(int NewLife);
	int GetLife();
	bool UsarChave;
	void OnDeath();
	void Pause();
	FORCEINLINE int GetNewLife() const { return Life; }
	FORCEINLINE void AMyCharacter::SetNewLife(int NewLife) { Life = NewLife; }
	FORCEINLINE TArray<class AChave1*> GetInventory() const { return Inventory; }
	FORCEINLINE TArray<class AObjeto*> GetObjeto() const { return Objeto; }
	TSubclassOf<class UUserWidget> UserWidget;

private:
	UPROPERTY(EditAnywhere)
		int ChavesGuardadas = 0;

	UPROPERTY(EditAnywhere)
		UCameraComponent* PlayerCamera;
	UPROPERTY(EditAnywhere)
		USpringArmComponent* CameraBoom;
    UPROPERTY(EditAnywhere)
		int Life = 3;
	UPROPERTY(EditAnywhere)
		UAnimSequence* JumpAnim;
	UPROPERTY(EditAnywhere)
	UAnimSequence* CrouchAnim;
		
		//UPROPERTY(EditAnywhere)
		//	int Pontuacao;

		//void ShowPontuacao();

		USphereComponent* CollectCollisionComp;
		TArray<class AChave1*> Inventory;
		TArray<class AObjeto*> Objeto;
		//TSubclassOf<class UUserWidget> UserWidget;

		//USoundCue* FireSound;
		//UAudioComponent* AudioComp;

	void MoveForward(float Value);
	void MoveRight(float Value);
	void StartRun();
	void StopRun();
	
	//void UsarObjeto();
	
	//void DropProjectile();
	//void Turn(float Value);
	void OnCollect();
	void OnCollect2();
	void AbrirPorta();
	//void Pause();

	//UFUNCTION(Reliable, Server, WithValidation)
	//	void DropProjectileServer();
	//void DropProjectileServer_Implementation();
	//bool DropProjectileServer_Validate();



	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);


};

