// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject2.h"
#include "MyCharacter.h"
#include "Item.h"
#include "Objeto.h"
#include "Chave1.h"
#include "Porta.h"
#include "EnemyCharacter.h"
#include "EnemyAI.h"
// Sets default values
AMyCharacter::AMyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->bGenerateOverlapEvents = true;
	GetCapsuleComponent()->bGenerateOverlapEvents = true;
	GetCapsuleComponent()->SetCollisionProfileName("BlockAllDynamic");
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &AMyCharacter::OnHit);
	//MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
	//MeshComp->SetCollisionProfileName("NoCollision");
	//MeshComp->AttachTo(GetCapsuleComponent());

	ConstructorHelpers::FObjectFinder<USkeletalMesh>
		SkeletalMesh(TEXT("SkeletalMesh'/Game/AnimStarterPack/UE4_Mannequin/Mesh/SK_Mannequin.SK_Mannequin'"));
	if (SkeletalMesh.Succeeded()) {
		GetMesh()->SetSkeletalMesh(SkeletalMesh.Object);
	}
	GetMesh()->SetWorldLocation(FVector(0.0f, 0.0f, -80.0f));
	GetMesh()->SetWorldScale3D(FVector(0.9f, 0.9f, 0.9f));
	GetMesh()->SetWorldRotation(FRotator(0.0f, -90.0f, 0.0f));
	GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	ConstructorHelpers::FObjectFinder<UAnimBlueprint>
		AnimObj(TEXT("AnimBlueprint'/Game/BluePrints/WalkAnimationBP.WalkAnimationBP'"));
	if (AnimObj.Succeeded()) {
		GetMesh()->SetAnimInstanceClass(AnimObj.Object->GetAnimBlueprintGeneratedClass());
	}

	ConstructorHelpers::FObjectFinder<UAnimSequence>
		AnimJumpLoad(TEXT("AnimSequence'/Game/AnimStarterPack/Jump_From_Stand.Jump_From_Stand'"));
	if (AnimJumpLoad.Succeeded()) {
		JumpAnim = AnimJumpLoad.Object;

	}
	ConstructorHelpers::FObjectFinder<UAnimSequence>
		AnimCrouchLoad(TEXT("AnimSequence'/Game/AnimStarterPack/Crouch_Idle_Rifle_Ironsights.Crouch_Idle_Rifle_Ironsights'"));
	if (AnimCrouchLoad.Succeeded()) {
		CrouchAnim = AnimCrouchLoad.Object;
	}
		PlayerCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PlayerCamera"));
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);
	PlayerCamera->AttachTo(CameraBoom);

	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;

	CollectCollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("CollectCollision"));
	CollectCollisionComp->InitSphereRadius(200.0f);
	CollectCollisionComp->AttachTo(RootComponent);
}

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();
	bool TemChave = false;
	bool TemObjeto = false;
}

// Called every frame
void AMyCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );


	if (GetMesh()->GetAnimationMode() == EAnimationMode::AnimationSingleNode
		&& GetCharacterMovement()->IsMovingOnGround() && CanCrouch() == true) {
		GetMesh()->SetAnimationMode(EAnimationMode::AnimationBlueprint);
	}


}

// Called to bind functionality to input



void AMyCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAxis("MoveForward", this, &AMyCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AMyCharacter::MoveRight);
	InputComponent->BindAxis("Turn", this, &ACharacter::AddControllerYawInput);

	InputComponent->BindAction("Jump", IE_Pressed, this, &AMyCharacter::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	InputComponent->BindAction("Run", IE_Pressed, this, &AMyCharacter::StartRun);
	InputComponent->BindAction("Run", IE_Released, this, &AMyCharacter::StopRun);
	InputComponent->BindAction("Crouch", IE_Released, this, &AMyCharacter::StopCrouch);
	InputComponent->BindAction("Crouch", IE_Pressed, this, &AMyCharacter::StartCrouch);
	InputComponent->BindAction("AbrirPorta", IE_Pressed, this, &AMyCharacter::AbrirPorta);
	//InputComponent->BindAction("Drop", IE_Pressed, this, &AMyCharacter::DropProjectile);
	InputComponent->BindAction("Collect", IE_Pressed, this, &AMyCharacter::OnCollect);
	InputComponent -> BindAction("Collect2", IE_Pressed, this, &AMyCharacter::OnCollect2);
	//InputComponent->BindAction("Pause", IE_Pressed, this, &AMyCharacter::Pause);
	//InputComponent->BindAction("Show", IE_Pressed, this, &AMyCharacter::ShowPontuacao);

}

void AMyCharacter::MoveForward(float Value) {
	//FVector Forward(0, 1, 0);
	//AddMovementInput(Forward, Value);

	if (Controller != nullptr && Value != 0) {
		FRotator Rotation = Controller->GetControlRotation();
		if (GetCharacterMovement()->IsMovingOnGround()) {
			Rotation.Pitch = 0.0f;
		}
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AMyCharacter::MoveRight(float Value) {
	//FVector Right(1, 0, 0);
	//AddMovementInput(Right, Value);

	if (Controller != nullptr && Value != 0.0f) {
		FRotator Rotation = Controller->GetControlRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}
void AMyCharacter::StartRun() {
	GetCharacterMovement()->MaxWalkSpeed = 800;
}

void AMyCharacter::StopRun() {
	GetCharacterMovement()->MaxWalkSpeed = 400;
}
void AMyCharacter::Jump() {
	Super::Jump();

	if (JumpAnim != nullptr) {
		GetMesh()->PlayAnimation(JumpAnim, false);
	}
}
void AMyCharacter::Crouch(bool bClientSimulation) {
	Super::Crouch();
	if (CrouchAnim != nullptr) {
		GetMesh()->PlayAnimation(CrouchAnim, false);
	}
}

void AMyCharacter::StartCrouch() {

	Crouch();
}
void AMyCharacter::StopCrouch() {
	UnCrouch();
}


void AMyCharacter::SetLife(int NewLife) {
	Life = NewLife;
}

int AMyCharacter::GetLife() {
	return Life;
}

void AMyCharacter::OnCollect() {
	TArray<AActor*> AtoresColetados;
	CollectCollisionComp->GetOverlappingActors(AtoresColetados);

	for (int i = 0; i < AtoresColetados.Num(); i++) {
		if (AtoresColetados[i]->IsA(AChave1::StaticClass())) {
			AChave1* ItemColetado = Cast<AChave1>(AtoresColetados[i]);
			Inventory.Add(ItemColetado);
			
			ItemColetado->Destroy();
			TemChave = true;
			ChavesGuardadas++;
			UE_LOG(LogTemp, Warning, TEXT("%d"), Inventory.Num());


		}
	}



}


void AMyCharacter::OnCollect2() {

	TArray<AActor*> ObjetoColetados;
	CollectCollisionComp->GetOverlappingActors(ObjetoColetados);

	for (int i = 0; i < ObjetoColetados.Num(); i++) {
		if (ObjetoColetados[i]->IsA(AObjeto::StaticClass())) {
			AObjeto* ObjetoPego = Cast<AObjeto>(ObjetoColetados[i]);
			Objeto.Add(ObjetoPego);
			ObjetoPego->Destroy();
			TemObjeto = true;
			UE_LOG(LogTemp, Warning, TEXT("%d"), Objeto.Num());
		}
	}
}

void

AMyCharacter::AbrirPorta() {


	if (ChavesGuardadas > 0) {
		TemChave = true;

		TArray<AActor*> Porta;
		CollectCollisionComp->GetOverlappingActors(Porta);

		for (int i = 0; i < Porta.Num(); i++) {
			if (Porta[i]->IsA(APorta::StaticClass())) {
				APorta* DoorOnCollision = Cast<APorta>(Porta[i]);
				DoorOnCollision->SetOpen(1);
				
				ChavesGuardadas --;
			}

			
		
		}
	}

}
void AMyCharacter::OnDeath() {
	if (Life <= 0) {
		FVector InitialLocation(-347.806793f, 197.234497f, 39.354198f);
		Life = 3;
		SetActorLocation(InitialLocation);
		UE_LOG(LogTemp, Warning, TEXT("Voce perdeu!"));
	}

}

UFUNCTION()
void AMyCharacter::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) {


	if (OtherActor->IsA(AEnemyCharacter::StaticClass())) {

		UE_LOG(LogTemp, Warning, TEXT("%d"),Life);
		Life--;
		OnDeath();

		//TESTE TESTE TESTE
	}
}